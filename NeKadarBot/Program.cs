﻿using LiteDB;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;

namespace NeKadarBot
{
    class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient(File.ReadAllText("Token.txt"));

        static ExchangeRate Koinim = new ExchangeRate();
        static ExchangeRate Paribu = new ExchangeRate();
        static ExchangeRate BTCTurk = new ExchangeRate();
        static ExchangeRate Koineks = new ExchangeRate();
        static ExchangeRate CoinTeb = new ExchangeRate();
        static ExchangeRate Ovis = new ExchangeRate();

        static ExchangeRate Poloniex = new ExchangeRate();
        static ExchangeRate Bitfinex = new ExchangeRate();
        static ExchangeRate Binance = new ExchangeRate();

        static System.Timers.Timer tmr = new System.Timers.Timer();

        static void Main(string[] args)
        {
            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            tmr.Elapsed += Tmr_Elapsed;
            tmr.Interval = 1000 * 30; // 30 seconds
            tmr.Start();

            Thread updateThread = new Thread(new ThreadStart(UpdateAsync));
            updateThread.Priority = ThreadPriority.Lowest;
            updateThread.Start();

            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            var me = Bot.GetMeAsync().Result;

            Console.Title = me.Username;

            Bot.StartReceiving();
            Console.ReadLine();
            Bot.StopReceiving();

            Console.ReadLine();
        }

        static bool restarted = false;

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Unhandled Exception: " + e.ExceptionObject.ToString());
            Console.ResetColor();
            //Console.WriteLine("Press Enter to continue");
            //Console.ReadLine();

            try
            {
                File.WriteAllText(System.AppDomain.CurrentDomain.BaseDirectory + "exception.log", string.Format("{1}{0}", Environment.NewLine, e.ExceptionObject.ToString()));
            }
            catch { }

            Thread.Sleep(2000);
            
            if (!restarted)
            {
                restarted = true;
                Process.Start(Assembly.GetExecutingAssembly().Location);
            }

            Environment.Exit(1);
        }

        private static void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread updateThread = new Thread(new ThreadStart(UpdateAsync));
            updateThread.Priority = ThreadPriority.Lowest;
            updateThread.Start();
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Bot Error: " + receiveErrorEventArgs.ApiRequestException.Message);
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            try
            {
                var message = messageEventArgs.Message;

                Console.WriteLine("Message received: " + message.Text + " from " + message.From.Id + " (" + message.From.Username + ") on " + message.Date);

                try
                {
                    if (!File.Exists("MessageLog.txt"))
                    {
                        File.CreateText("MessageLog.txt");
                        Thread.Sleep(1000);
                    }

                    if (!File.Exists("Users.txt"))
                    {
                        File.CreateText("Users.txt");
                        Thread.Sleep(1000);
                    }

                    File.AppendAllText("MessageLog.txt", string.Format("{1},{2},{3},{4},{5}{0}", Environment.NewLine, message.From.Id, message.From.IsBot, message.From.Username, message.Text, message.Date));
                    File.AppendAllText("Users.txt", string.Format("{1}{0}", Environment.NewLine, message.From.Id));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + ex.Message);
                    Console.ResetColor();
                }

                if (message == null || message.Type != MessageType.TextMessage) return;

                await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                #region /getir
                if (message.Text.StartsWith("/announcement"))
                {
                    try
                    {
                        if (message.From.Username == "mertsarac")
                        {
                            string[] users = File.ReadAllLines("Users.txt");

                            foreach (string user in users)
                            {
                                long identifier = Convert.ToInt64(user);
                                await Bot.SendTextMessageAsync(new Telegram.Bot.Types.ChatId(identifier), message.Text.Replace("/announcement ", ""), ParseMode.Markdown);
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Bu işlem için yetkiniz bulunmamaktadır.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: " + ex.Message);
                        Console.ResetColor();
                    }
                }
                else if (message.Text.StartsWith("/notify"))
                {
                    try
                    {
                        string amount = string.Empty;

                        try
                        {
                            amount = message.Text.Split(' ')[1] + message.Text.Split(' ')[2];

                            var inlineKeyboard = new InlineKeyboardMarkup(new[]
                            {
                            new []
                            {
                                InlineKeyboardButton.WithCallbackData("Koinim", "Koinim|" + amount),
                                InlineKeyboardButton.WithCallbackData("Paribu", "Paribu|" + amount),
                            },
                            new []
                            {
                                InlineKeyboardButton.WithCallbackData("BTCTurk", "BTCTurk|" + amount),
                                InlineKeyboardButton.WithCallbackData("Koineks", "Koineks|" + amount),
                            },
                            new []
                            {
                                InlineKeyboardButton.WithCallbackData("CoinTeb", "CoinTeb|" + amount),
                                InlineKeyboardButton.WithCallbackData("Poloniex", "Poloniex|" + amount),
                            },
                            new []
                            {
                                InlineKeyboardButton.WithCallbackData("Bitfinex", "Bitfinex|" + amount)
                            }
                        });

                            await Bot.SendTextMessageAsync(message.Chat.Id, "Borsa seçimi yapınız", replyMarkup: inlineKeyboard);
                        }
                        catch 
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Lütfen tutar giriniz.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: " + ex.Message);
                        Console.ResetColor();
                    }
                }
                else if (message.Text.StartsWith("/feedback"))
                {
                    try
                    {
                        if (!File.Exists("Feedbacks.txt"))
                        {
                            File.CreateText("Feedbacks.txt");
                            Thread.Sleep(1000);
                        }

                        File.AppendAllText("Feedbacks.txt", string.Format("{2}: {1}{0}", Environment.NewLine, message.Text, message.From.Id));

                        await Bot.SendTextMessageAsync(message.Chat.Id, "Öneriniz kaydedildi. Geri bildiriminiz için teşekkür ederiz.");
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: " + ex.Message);
                        Console.ResetColor();
                    }
                }
                else if (message.Text.StartsWith("/getir"))
                {
                    if (message.Text.ToLower().Contains("koinim"))
                    {
                        if (Koinim.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Koinim*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Koinim.Buy, Koinim.Sell, Koinim.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Koinim fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("paribu"))
                    {
                        if (Paribu.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Paribu*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Paribu.Buy, Paribu.Sell, Paribu.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Paribu fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("btcturk"))
                    {
                        if (BTCTurk.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*BTCTurk*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, BTCTurk.Buy, BTCTurk.Sell, BTCTurk.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel BTCTurk fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("btctürk"))
                    {
                        if (BTCTurk.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*BTCTurk*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, BTCTurk.Buy, BTCTurk.Sell, BTCTurk.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel BTCTurk fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("btctürk"))
                    {
                        if (BTCTurk.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*BTCTurk*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, BTCTurk.Buy, BTCTurk.Sell, BTCTurk.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel BTCTurk fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("koineks"))
                    {
                        if (Koineks.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Koineks*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Koineks.Buy, Koineks.Sell, Koineks.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Koineks fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("cointeb"))
                    {
                        if (CoinTeb.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*CoinTeb*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, CoinTeb.Buy, CoinTeb.Sell, CoinTeb.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel CoinTeb fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("poloniex"))
                    {
                        if (Poloniex.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Poloniex*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Poloniex.Buy.ToString("0.##"), Poloniex.Sell.ToString("0.##"), Poloniex.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Poloniex fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("bitfinex"))
                    {
                        if (Bitfinex.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Bitfinex*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Bitfinex.Buy, Bitfinex.Sell, Bitfinex.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Bitfinex fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("ovis"))
                    {
                        if (Ovis.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Ovis*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Ovis.Buy, Ovis.Sell, Ovis.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Ovis fiyatları alınamamaktadır.");
                        }
                    }
                    else if (message.Text.ToLower().Contains("binance"))
                    {
                        if (Binance.LastUpdate != DateTime.MinValue)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, string.Format("*Binance*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Binance.Buy, Binance.Sell, Binance.LastUpdate), ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Binance fiyatları alınamamaktadır.");
                        }
                    }
                    else
                    {
                        string msg = string.Empty;

                        if (Koinim.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Koinim*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Koinim.Buy, Koinim.Sell, Koinim.LastUpdate);
                        }

                        if (Paribu.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Paribu*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Paribu.Buy, Paribu.Sell, Paribu.LastUpdate);
                        }

                        if (BTCTurk.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*BTCTurk*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, BTCTurk.Buy, BTCTurk.Sell, BTCTurk.LastUpdate);
                        }

                        if (Koineks.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Koineks*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Koineks.Buy, Koineks.Sell, Koineks.LastUpdate);
                        }

                        if (CoinTeb.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*CoinTeb*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, CoinTeb.Buy, CoinTeb.Sell, CoinTeb.LastUpdate);
                        }

                        if (Ovis.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Ovis*{0}Alış: {1} TL{0}Satış: {2} TL{0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Ovis.Buy, Ovis.Sell, Ovis.LastUpdate);
                        }

                        if (Poloniex.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Poloniex*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Poloniex.Buy.ToString("0.##"), Poloniex.Sell.ToString("0.##"), Poloniex.LastUpdate);
                        }

                        if (Bitfinex.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Bitfinex*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Bitfinex.Buy, Bitfinex.Sell, Bitfinex.LastUpdate);
                        }

                        if (Binance.LastUpdate != DateTime.MinValue)
                        {
                            msg += string.Format("*Binance*{0}Alış: {1} ${0}Satış: {2} ${0}Son Güncelleme: {3}{0}{0}", Environment.NewLine, Binance.Buy, Binance.Sell, Binance.LastUpdate);
                        }

                        if (msg != string.Empty)
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, msg, ParseMode.Markdown);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda borsalardan kur bilgisi alınamamaktadır. Lütfen daha sonra tekrar deneyiniz.");
                        }
                    }
                }
                #endregion
                else if (message.Text.StartsWith("/koinim"))
                {
                    if (Koinim.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Koinim kurlarıyla *" + input + " TL = " + CalculateBTC(input, Koinim) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Koinim kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Koinim) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Koinim fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/paribu"))
                {
                    if (Paribu.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Paribu kurlarıyla *" + input + " TL = " + CalculateBTC(input, Paribu) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Paribu kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Paribu) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Paribu fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/btcturk"))
                {
                    if (BTCTurk.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli BTCTurk kurlarıyla *" + input + " TL = " + CalculateBTC(input, BTCTurk) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli BTCTurk kurlarıyla *" + input + " BTC = " + CalculateTRY(input, BTCTurk) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel BTCTurk fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/koineks"))
                {
                    if (Koineks.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Koineks kurlarıyla *" + input + " TL = " + CalculateBTC(input, Koineks) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli BTCTurk kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Koineks) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Koineks fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/cointeb"))
                {
                    if (CoinTeb.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli CoinTeb kurlarıyla *" + input + " TL = " + CalculateBTC(input, CoinTeb) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli CoinTeb kurlarıyla *" + input + " BTC = " + CalculateTRY(input, CoinTeb) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel CoinTeb fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/ovis"))
                {
                    if (Ovis.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("tl") || message.Text.ToLower().Contains("try"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Ovis kurlarıyla *" + input + " TL = " + CalculateBTC(input, Ovis) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Koinim.LastUpdate + " tarihli Ovis kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Ovis) + " TL*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Ovis fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/poloniex"))
                {
                    if (Poloniex.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("usd") || message.Text.ToLower().Contains("$"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Poloniex.LastUpdate + " tarihli Poloniex kurlarıyla *" + input + " $ = " + CalculateBTC(input, Poloniex) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Poloniex.LastUpdate + " tarihli Poloniex kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Poloniex) + " $*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Poloniex fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/bitfinex"))
                {
                    if (Bitfinex.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("usd") || message.Text.ToLower().Contains("$"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Bitfinex.LastUpdate + " tarihli Bitfinex kurlarıyla *" + input + " $ = " + CalculateBTC(input, Bitfinex) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Bitfinex.LastUpdate + " tarihli Bitfinex kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Bitfinex) + " $*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Bitfinex fiyatları alınamamaktadır.");
                    }
                }
                else if (message.Text.StartsWith("/binance"))
                {
                    if (Binance.LastUpdate != DateTime.MinValue)
                    {
                        if (message.Text.ToLower().Contains("usd") || message.Text.ToLower().Contains("$"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Binance.LastUpdate + " tarihli Binance kurlarıyla *" + input + " $ = " + CalculateBTC(input, Binance) + " BTC*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else if (message.Text.ToLower().Contains("btc"))
                        {
                            double input;

                            if (double.TryParse(message.Text.Replace(".", ",").Split(new char[0])[1], out input))
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, Binance.LastUpdate + " tarihli Binance kurlarıyla *" + input + " BTC = " + CalculateTRY(input, Binance) + " $*" + Environment.NewLine + "⚠️ Borsa komisyonları hesaba dahil edilmemiştir.", ParseMode.Markdown, false, false, message.MessageId);
                            }
                            else
                            {
                                await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz miktar tanınamadı.");
                            }
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(message.Chat.Id, "Girdiğiniz değerler tanınamadı.");
                        }
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Şu anda güncel Binance fiyatları alınamamaktadır.");
                    }
                }
                else
                {
                    var usage = @"Kullanım:
/getir       - Borsaların en güncel kur bilgilerini listeler. Boşluk bırakıp borsa adını yazdığınızda seçtiğiniz borsanın kur bilgilerini döndürür.
/koinim      - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Koinim'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/btcturk     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda BTCTurk'ün anlık alış-satış fiyatları üzerinden hesaplama yapar.
/paribu      - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Paribu'nun anlık alış-satış fiyatları üzerinden hesaplama yapar.
/koineks     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Koineks'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/cointeb     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda CoinTeb'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/ovis        - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Ovis'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/poloniex    - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Poloniex'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/bitfinex    - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Bitfinex'in anlık alış-satış fiyatları üzerinden hesaplama yapar.

Örnek BTC Hesaplama:
/koinim 100 TL

Örnek TL Hesaplama:
/koinim 0.015 BTC
";

                    await Bot.SendTextMessageAsync(message.Chat.Id, usage);
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: " + ex.Message);
                Console.ResetColor();

                try
                {
                    var usage = @"*Mesajınız tanınamadı!*
Kullanım:
/getir       - Borsaların en güncel kur bilgilerini listeler. Boşluk bırakıp borsa adını yazdığınızda seçtiğiniz borsanın kur bilgilerini döndürür.
/koinim      - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Koinim'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/btcturk     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda BTCTurk'ün anlık alış-satış fiyatları üzerinden hesaplama yapar.
/paribu      - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Paribu'nun anlık alış-satış fiyatları üzerinden hesaplama yapar.
/koineks     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Koineks'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/cointeb     - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda CoinTeb'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/ovis        - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Ovis'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/poloniex    - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Poloniex'in anlık alış-satış fiyatları üzerinden hesaplama yapar.
/bitfinex    - Boşluk bırakıp çevireceğiniz para miktarını yazdığınızda Bitfinex'in anlık alış-satış fiyatları üzerinden hesaplama yapar.

Örnek BTC Hesaplama:
/koinim 100 TL

Örnek TL Hesaplama:
/koinim 0.015 BTC
";

                    await Bot.SendTextMessageAsync(messageEventArgs.Message.Chat.Id, usage);
                }
                catch (Exception ex2)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + ex2.Message);
                    Console.ResetColor();
                }
            }
        }

        static string CalculateBTC(double input, ExchangeRate rate)
        {
            return ((1 / rate.Sell) * input).ToString("0.########");
        }

        static string CalculateTRY(double input, ExchangeRate rate)
        {
            return (rate.Buy * input).ToString("0.##");
        }

        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            if (callbackQueryEventArgs.CallbackQuery.Data.Contains("|"))
            {
                string amount = callbackQueryEventArgs.CallbackQuery.Data.Split('|')[1];

                switch (callbackQueryEventArgs.CallbackQuery.Data.Split('|')[0])
                {
                    case "Koinim":
                        var inlineKeyboard = new InlineKeyboardMarkup(new[]
                            {
                            new []
                            {
                                InlineKeyboardButton.WithCallbackData("Yüseliş", "Koinim-Yuksel-" + amount),
                                InlineKeyboardButton.WithCallbackData("Düşüş", "Koinim-Dusus-" + amount),
                            }
                        });
                        await Bot.SendTextMessageAsync(callbackQueryEventArgs.CallbackQuery.Message.Chat.Id, "Bildirim Tipini Seçiniz", replyMarkup: inlineKeyboard);
                        break;
                }
            }
            else
            {
                string market = callbackQueryEventArgs.CallbackQuery.Data.Split('-')[0];
                string type = callbackQueryEventArgs.CallbackQuery.Data.Split('-')[1];
                string amount = callbackQueryEventArgs.CallbackQuery.Data.Split('-')[2];
                NotificationStyle notificationType;

                switch (type)
                {
                    case "Yuksel":
                        notificationType = NotificationStyle.Higher;
                        break;
                    case "Dusus":
                        notificationType = NotificationStyle.Lower;
                        break;
                    default:
                        notificationType = NotificationStyle.Higher;
                        break;
                }

                RegisterNotification(new Notificate
                {
                    Amount = Convert.ToDouble(amount.Replace("TL", "").Replace("$", "").Replace("USD", "")),
                    IsActive = true,
                    UserId = Convert.ToInt64(callbackQueryEventArgs.CallbackQuery.Id),
                    Market = market,
                    NotificationType = notificationType
                });

                await Bot.AnswerCallbackQueryAsync(callbackQueryEventArgs.CallbackQuery.Id, "Kaydınız alındı!");
            }
        }

        static void UpdateAsync()
        {
            try
            {
                Thread KoinimThread = new Thread(new ThreadStart(UpdateKoinim));
                KoinimThread.Priority = ThreadPriority.Lowest;

                Thread ParibuThread = new Thread(new ThreadStart(UpdateParibu));
                ParibuThread.Priority = ThreadPriority.Lowest;

                Thread BTCTurkThread = new Thread(new ThreadStart(UpdateBTCTurk));
                BTCTurkThread.Priority = ThreadPriority.Lowest;

                Thread KoineksThread = new Thread(new ThreadStart(UpdateKoineks));
                KoineksThread.Priority = ThreadPriority.Lowest;

                Thread CoinTebThread = new Thread(new ThreadStart(UpdateCoinTeb));
                CoinTebThread.Priority = ThreadPriority.Lowest;

                Thread PoloniexThread = new Thread(new ThreadStart(UpdatePoloniex));
                PoloniexThread.Priority = ThreadPriority.Lowest;

                Thread BitfinexThread = new Thread(new ThreadStart(UpdateBitfinex));
                BitfinexThread.Priority = ThreadPriority.Lowest;

                Thread BinanceThread = new Thread(new ThreadStart(UpdateBinance));
                BinanceThread.Priority = ThreadPriority.Lowest;

                Thread OvisThread = new Thread(new ThreadStart(UpdateOvis));
                OvisThread.Priority = ThreadPriority.Lowest;

                Thread NotificationsThread = new Thread(new ThreadStart(CheckNotifications));
                NotificationsThread.Priority = ThreadPriority.Lowest;

                KoinimThread.Start();
                ParibuThread.Start();
                BTCTurkThread.Start();
                //KoineksThread.Start();
                CoinTebThread.Start();
                PoloniexThread.Start();
                BitfinexThread.Start();
                BinanceThread.Start();
                OvisThread.Start();
                //NotificationsThread.Start();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Update Error: " + ex.Message);
                Console.ResetColor();
            }
        }

        static void UpdateKoinim()
        {
            var client = new RestClient("https://koinim.com/ticker/");
            client.Timeout = Int32.MaxValue;
            var request = new RestRequest(Method.GET);

            request.AddHeader("authority", "koinim.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("upgrade-insecure-requests", "1");
            request.AddHeader("accept-language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("accept-encoding", "gzip, deflate, br");
            request.AddHeader("dnt", "1");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Koinim.Buy = json.buy.Value;
                Koinim.Sell = json.sell.Value;
                Koinim.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdateParibu()
        {
            var client = new RestClient("http://paribu.com/ticker");
            client.Timeout = Int32.MaxValue;
            var request = new RestRequest(Method.GET);

            request.AddHeader("Referer", "http://paribu.com/ticker");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("Upgrade-Insecure-Requests", "1");
            request.AddHeader("Accept-Language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("DNT", "1");
            request.AddHeader("Cookie", "__cfduid=d5fd3c8dc58db2033e5394a933a77bbe91512237662; _ga=GA1.2.2002453985.1512972855; cf_clearance=d7db553fc8dadd5fae3b176f1ee0935b2481b23d-1514330270-604800; XSRF-TOKEN=eyJpdiI6IldMZjVkUnJSZDNSWVU3WWlPQ1A5Z2c9PSIsInZhbHVlIjoiTHByY0lGd2MyVVM4T012Z0NRNXdaKzI0TmlMMFNEMlJjdEx2ZUV5Qlg3a0lLY293UFBEK1NPK2FkTXpubGlWeDdLT0I5OGFDUk51TWdPRXpCK29QbXc9PSIsIm1hYyI6IjU4YzZlZGMzYTQ5ZWY3YzE0ZTRiNmMxZjE3NDY2YjkyN2JmMDdiMDc5MTU1NGNhNDAwNGEzMDEyZDAxMjZlZjcifQ^%^3D^%^3D; laravel_session=eyJpdiI6ImJcL0tWYktwMGJYcnNpK1YrVmJmOFd3PT0iLCJ2YWx1ZSI6Ikx2N3d5c3JESndcL3lFbGNSZVpiTUJ4UTkrem5xK2k1MEV0Y2ZXQ05kdmc2Wm11Q0x4R3hBR0wwQmEwOFo2bExudUxOSTcwY2Rhb1M3ZHk1aFwvOHFQSmc9PSIsIm1hYyI6IjViMTczMTBkNDQ2ODJlMDZjZGU4MmQwYjQ2NTYzM2JmMjJmZDhhNDM4YjJkNjY3NWQzZDU2OWQwNGYxOTQ1NWEifQ^%^3D^%^3D");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Paribu.Buy = json.BTC_TL.last.Value;
                Paribu.Sell = json.BTC_TL.lowestAsk.Value;
                Paribu.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdateBTCTurk()
        {
            var client = new RestClient("https://www.btcturk.com/api/ticker");
            client.Timeout = Int32.MaxValue;
            var request = new RestRequest(Method.GET);

            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("authority", "www.btcturk.com");
            request.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("upgrade-insecure-requests", "1");
            request.AddHeader("accept-language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("accept-encoding", "gzip, deflate, br");
            request.AddHeader("dnt", "1");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                BTCTurk.Buy = json[0].last.Value;
                BTCTurk.Sell = json[0].ask.Value;
                BTCTurk.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdateKoineks()
        {
            var client = new RestClient("https://koineks.com/ticker");
            client.Timeout = Int32.MaxValue;
            var request = new RestRequest(Method.GET);

            request.AddHeader("cookie", "__cfduid=d9cae9c53e71b990271e208dc4ae024e31518201002; _ga=GA1.2.549294081.1518201000; XSRF-TOKEN=eyJpdiI6Im5MQk04ektZYUZiVTUyb1dHS2hlWEE9PSIsInZhbHVlIjoiQlp5SFJnUWJ0eElJcDd3KzJ4d0w4K2txUHlja0M5TjBZODdwdnIxRkVDT3ZMYThCeG54Y24rUDVWRFwvUGl0MUdNTk1pNml2dHEwdUpwakVqbklBN1RnPT0iLCJtYWMiOiIwOWM0ODViOTNlOTdmYzAwYjQzMDBmZWIwYmY2YjVjNGRjMzQyNzQ2NjhhYTMwMDFmYTFmNDI4NmEyZjdmYTRmIn0%3D; koineks_session=eyJpdiI6Ik5xNzlQWmlkVGo4ZUhrdStRbnljWmc9PSIsInZhbHVlIjoiUldmWW1sQ0tkS0lVODBOYmRtVkMzUGpFS0tHYTJUZmUycGFzVzBYa29Hd3NDUEQyVWhHb1QxTzdpbHRhRUVnU0pnd3ZjcW5hYnBIRDM3ZHMxYVBIb3c9PSIsIm1hYyI6ImU5OTVhZDRhYWUyZTFmYzU1ZDkxNzg1OGRjNjhiMjFhMjQ1MDZlZGIyNWVmM2ZlN2VkYWEyYWZlMDliOTQ1YTkifQ%3D%3D");
            request.AddHeader("authority", "koineks.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("upgrade-insecure-requests", "1");
            request.AddHeader("accept-language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("accept-encoding", "gzip, deflate, br");
            request.AddHeader("dnt", "1");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Koineks.Buy = Convert.ToDouble(json.BTC.bid.Value.Replace(".", ","));
                Koineks.Sell = Convert.ToDouble(json.BTC.ask.Value.Replace(".", ","));
                Koineks.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdateCoinTeb()
        {

        }

        static void UpdateBinance()
        {

        }

        static void UpdateOvis()
        {
            var client = new RestClient("https://api.ovis.com.tr/Public/Tickers");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cache-Control", "no-cache");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Ovis.Buy = Convert.ToDouble(json.data[0].bid.Value.Replace(".", ","));
                Ovis.Sell = Convert.ToDouble(json.data[0].ask.Value.Replace(".", ","));
                Ovis.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdatePoloniex()
        {
            var client = new RestClient("https://poloniex.com/public?command=returnTicker");
            var request = new RestRequest(Method.GET);

            request.AddHeader("authority", "poloniex.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("upgrade-insecure-requests", "1");
            request.AddHeader("accept-language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("accept-encoding", "gzip, deflate, br");
            request.AddHeader("dnt", "1");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Poloniex.Buy = Convert.ToDouble(json.USDT_BTC.highestBid.Value.Replace(".", ","));
                Poloniex.Sell = Convert.ToDouble(json.USDT_BTC.last.Value.Replace(".", ","));
                Poloniex.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void UpdateBitfinex()
        {
            var client = new RestClient("https://api.bitfinex.com/v1/pubticker/btcusd");
            var request = new RestRequest(Method.GET);

            request.AddHeader("authority", "api.bitfinex.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            request.AddHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
            request.AddHeader("upgrade-insecure-requests", "1");
            request.AddHeader("accept-language", "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7");
            request.AddHeader("accept-encoding", "gzip, deflate, br");
            request.AddHeader("dnt", "1");
            request.AddHeader("pragma", "no-cache");

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response.Content);

                Bitfinex.Buy = Convert.ToDouble(json.bid.Value.Replace(".", ","));
                Bitfinex.Sell = Convert.ToDouble(json.ask.Value.Replace(".", ","));
                Bitfinex.LastUpdate = DateTime.Now.AddHours(3);
            }
        }

        static void RegisterNotification(Notificate notification)
        {
            using (var db = new LiteDatabase(@"Notifications.db"))
            {
                var notifications = db.GetCollection<Notificate>("Notifications");
                notifications.Insert(notification);
            }
        }

        static void CheckNotifications()
        {
            using (var db = new LiteDatabase(@"Notifications.db"))
            {
                var notifications = db.GetCollection<Notificate>("Notifications");
                IEnumerable<Notificate> results = notifications.Find(x => x.IsActive == true);
                
                foreach (Notificate result in results)
                {
                    switch (result.Market)
                    {
                        case "Koinim":
                            if (result.NotificationType == NotificationStyle.Higher)
                            {
                                if (Koinim.Buy >= result.Amount)
                                {
                                    Bot.SendTextMessageAsync(result.UserId, "Alarm: Koinim borsası için belirttiğiniz eşik aşıldı.").GetAwaiter().GetResult();
                                }
                            }
                            else if (result.NotificationType == NotificationStyle.Lower)
                            {
                                if (Koinim.Buy <= result.Amount)
                                {
                                    Bot.SendTextMessageAsync(result.UserId, "Alarm: Koinim borsası için belirttiğiniz eşik aşıldı.").GetAwaiter().GetResult();
                                }
                            }
                            break;
                        case "Paribu":
                            if (result.NotificationType == NotificationStyle.Higher)
                            {
                                if (Paribu.Buy >= result.Amount)
                                {
                                    Bot.SendTextMessageAsync(result.UserId, "Alarm: Paribu borsası için belirttiğiniz eşik aşıldı.").GetAwaiter().GetResult();
                                }
                            }
                            else if (result.NotificationType == NotificationStyle.Lower)
                            {
                                if (Paribu.Buy <= result.Amount)
                                {
                                    Bot.SendTextMessageAsync(result.UserId, "Alarm: Paribu borsası için belirttiğiniz eşik aşıldı.").GetAwaiter().GetResult();
                                }
                            }
                            break;
                    }
                }
            }
        }

        class ExchangeRate
        {
            public double Buy { get; set; }
            public double Sell { get; set; }
            public DateTime LastUpdate { get; set; }
        }

        class Notificate
        {
            public int id { get; set; }
            public Int64 UserId { get; set; }
            public string Market { get; set; }
            public NotificationStyle NotificationType { get; set; }
            public double Amount { get; set; }
            public bool IsActive { get; set; }
        }

        enum NotificationStyle
        {
            Lower = 0,
            Higher = 1
        }
    }
}